package program;

public record Order(int clientId, String orderId, double startTime) {
}