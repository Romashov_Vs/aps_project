package program.events;

public enum EventType {
  GENERATED,
  PROGRESS,
  COMPLETED,
  ERROR
}
