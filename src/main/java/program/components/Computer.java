package program.components;

import lombok.Getter;
import lombok.Setter;
import program.Order;
import statistic.Statistics;
import org.jetbrains.annotations.NotNull;

import java.util.Random;

@Getter
@Setter
public class Computer {
  private final Statistics statistics;
  private int computerId;
  private Order currentOrder;
  private double orderStartTime;

  public Computer(final int deviceId, @NotNull final Statistics statistics) {
    this.computerId = deviceId;
    this.orderStartTime = 0;
    this.currentOrder = null;
    this.statistics = statistics;
  }

  public double getReleaseTime() {
    Random rand = new Random();
    return Statistics.minimum + rand.nextDouble() * (Statistics.maximum - Statistics.minimum);
  }

  public boolean isFree() {
    return (currentOrder == null);
  }

  public void release(final double currentTime) {
    final double timeOnComputer = currentTime - orderStartTime;
    statistics.orderCompleted(currentOrder.clientId(), timeOnComputer, timeOnComputer);
    statistics.addComputerByClientTime(currentOrder.clientId(), timeOnComputer);
    statistics.addEachComputerTime(computerId, timeOnComputer);
    currentOrder = null;
    orderStartTime = currentTime;
  }
}
