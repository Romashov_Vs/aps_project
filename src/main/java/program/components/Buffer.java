package program.components;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import program.Controller;
import program.Order;

import java.util.ArrayList;

@Getter
public class Buffer {
  @NotNull
  private final ArrayList<Order> orders;
  private Order canceledOrder;
  private final int capacity;
  private int insertIndex;
  private int size;
  private final Boolean[] elementsToPackage;

  public Buffer(final int capacity) {
    this.capacity = capacity;
    this.size = 0;
    this.insertIndex = 0;
    orders = new ArrayList<>(capacity);
    for (int i = 0; i < capacity; i++) {
      orders.add(null);
    }
    elementsToPackage = new Boolean[capacity];
    for (int i = 0; i < capacity; i++) {
      elementsToPackage[i] = false;
    }
    canceledOrder = new Order(0, "-", 0);
  }

  public boolean isEmpty() {
    return size == 0;
  }

  public boolean isFull() {
    return size == capacity;
  }

  public Integer findMinIdCustomer(){
    Integer minIdCustomer = Integer.MAX_VALUE;
    for (Order order : orders) {
      if (order != null && minIdCustomer > order.clientId()) {
        minIdCustomer = order.clientId();
      }
    }
    return minIdCustomer;
  }

  public Integer findMaxIdCustomer(){
    Integer minIdCustomer = Integer.MIN_VALUE;
    for (Order order : orders) {
      if (order != null && minIdCustomer < order.clientId()) {
        minIdCustomer = order.clientId();
      }
    }
    return minIdCustomer;
  }

  public void addOrder(@NotNull final Order order) {
    if (isFull()) {
      Integer maxIdCustomer = findMaxIdCustomer();
      while (orders.get(insertIndex).clientId() != maxIdCustomer) {
        insertIndex = (insertIndex + 1) % capacity;
      }

      canceledOrder = orders.get(insertIndex);
      orders.set(insertIndex, order);
      elementsToPackage[insertIndex] = false;
      insertIndex = (insertIndex + 1) % capacity;
      makeStatistics(canceledOrder, order);
    } else {
      canceledOrder = new Order(0, "-", 0);
      while (orders.get(insertIndex) != null) {
        insertIndex = (insertIndex + 1) % capacity;
      }
      orders.set(insertIndex, order);
      insertIndex = (insertIndex + 1) % capacity;
      size++;
    }
  }

  public Order getOrder() {
    if (isEmpty()) {
      throw new RuntimeException("Buffer is empty!");
    }
    if (isElementInQueueToSend()) {
      return sendOrder();
    } else {
      Integer minIdCustomer = findMinIdCustomer();
      for (int i = 0; i < orders.size(); i++) {
        if (orders.get(i) != null && orders.get(i).clientId() == minIdCustomer) {
          elementsToPackage[i] = true;
        }
      }
      if (isElementInQueueToSend()) {
        return sendOrder();
      }
    }
    throw new RuntimeException("Buffer is empty!");
  }

  private Order sendOrder() {
    double minStartTime = Double.MAX_VALUE;
    int indexToDelete = Integer.MAX_VALUE;
    for (int i = 0; i < orders.size(); i++) {
      if (elementsToPackage[i] && minStartTime > orders.get(i).startTime()) {
        minStartTime = orders.get(i).startTime();
        indexToDelete = i;
      }
    }
    size--;
    Order order = orders.get(indexToDelete);
    orders.set(indexToDelete, null);
    elementsToPackage[indexToDelete] = false;
    return order;
  }

  private void makeStatistics(@NotNull final Order canceledOrder, @NotNull final Order order) {
    Controller.statisticsPub.orderCanceled(canceledOrder.clientId(),
            order.startTime() - canceledOrder.startTime());
  }

  boolean isElementInQueueToSend() {
    for (Boolean element : elementsToPackage) {
      if (element) {
        return true;
      }
    }
    return false;
  }
}