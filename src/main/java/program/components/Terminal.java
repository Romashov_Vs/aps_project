package program.components;

import program.Order;
import program.events.Event;
import program.events.EventType;
import statistic.Statistics;

import java.util.ArrayList;
import java.util.List;

public class Terminal {
  private final Buffer buffer;
  private final List<Client> clients;
  private final Statistics statistics;

  public Terminal(final Buffer buffer,
                  final List<Client> clients,
                  final Statistics statistics) {
    this.buffer = buffer;
    this.clients = clients;
    this.statistics = statistics;
  }

  private Order receiveOrder(final int currentId, final double currentTime) {
    return clients.get(currentId).generateOrder(currentTime);
  }

  public List<Event> putOrderToBuffer(final int currentId, final double currentTime) {
    final Order currentOrder = receiveOrder(currentId, currentTime);
    buffer.addOrder(currentOrder);
    List<Event> events = new ArrayList<>();
    events.add(new Event(EventType.PROGRESS, currentTime, currentOrder.orderId()));
    final double nextOrderTime = currentTime + clients.get(currentId).getNextOrderGenerationTime();
    if (nextOrderTime < Statistics.workTime) {
      events.add(new Event(EventType.GENERATED, nextOrderTime, null, currentId));
    }
    statistics.orderGenerated(currentId);
    return events;
  }
}
