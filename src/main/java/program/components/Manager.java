package program.components;

import lombok.Getter;
import program.Order;
import program.events.Event;
import program.events.EventType;
import statistic.Statistics;

import java.util.List;

@Getter
public class Manager {
  private final List<Computer> computers;
  private final Buffer buffer;
  private final Statistics statistics;
  private int currentIndex;

  public Manager(final Buffer buffer,
                 final List<Computer> computers,
                 final Statistics statistics) {
    this.buffer = buffer;
    this.computers = computers;
    this.statistics = statistics;
  }

  public Event sendOrderToDevice(final double currentTime, final Event currentEvent) {
    findFreeDeviceIndex();
    Computer currentComputer = computers.get(currentIndex);
    currentEvent.setOrderId("");
    if (currentComputer.isFree() && !buffer.isEmpty()) {
      currentEvent.setId(computers.get(currentIndex).getComputerId());
      final Order order = buffer.getOrder();
      currentEvent.setOrderId(order.orderId());
      computers.get(currentIndex).setCurrentOrder(order);
      computers.get(currentIndex).setOrderStartTime(currentTime);
      return new Event(
        EventType.COMPLETED,
        currentTime + computers.get(currentIndex).getReleaseTime(),
        order.orderId(),
        currentComputer.getComputerId());
    }
    return null;
  }

  public void findFreeDeviceIndex() {
    currentIndex = 0;
    while (!computers.get(currentIndex).isFree()) {
      currentIndex++;
      if (currentIndex == statistics.getComputersCount()) {
        currentIndex = 0;
        return;
      }
    }
  }
}
