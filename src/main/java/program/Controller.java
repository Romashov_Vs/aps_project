package program;

import lombok.Getter;
import program.components.*;
import program.events.Event;
import program.events.EventType;
import statistic.Statistics;

import java.util.ArrayList;
import java.util.List;

@Getter
public class Controller {
  public static Statistics statisticsPub = Statistics.getInstance();
  private final double workTime = Statistics.workTime;
  private Event currentEvent;
  private double currentTime;

  private final Statistics statistics;
  private final Buffer buffer;
  private final Terminal terminal;
  private final Manager manager;
  private final ArrayList<Client> clients;
  private final ArrayList<Computer> computers;
  private ArrayList<Event> events;

  private void initEvents() {
    events = new ArrayList<>();
    for (int i = 0; i < statisticsPub.getClientsCount(); i++) {
      events.add(new Event(EventType.GENERATED, clients.get(i).getNextOrderGenerationTime(), (i + "-" + 0), i));
    }
    if (events.size() > 0) {
      events.sort(Event::compareByTime);
    }
  }

  public Controller() {
    statisticsPub = Statistics.getInstance();
    currentTime = 0;
    buffer = new Buffer(statisticsPub.getBufferSize());
    computers = new ArrayList<>(statisticsPub.getComputersCount());
    for (int i = 0; i < statisticsPub.getComputersCount(); i++) {
      computers.add(new Computer(i, statisticsPub));
    }
    manager = new Manager(buffer, computers, statisticsPub);
    clients = new ArrayList<>(statisticsPub.getClientsCount());
    for (int i = 0; i < statisticsPub.getClientsCount(); i++) {
      clients.add(new Client(i));
    }
    terminal = new Terminal(buffer, clients, statisticsPub);
    statistics = statisticsPub;
    initEvents();
  }

  public void stepMode() {
    currentEvent = events.remove(0);
    final EventType currentType = currentEvent.eventType;
    final int currentId = currentEvent.id;
    currentTime = currentEvent.eventTime;
    if (currentType == EventType.GENERATED) {
      List<Event> newEvents = terminal.putOrderToBuffer(currentId, currentTime);
      if (!events.isEmpty()) {
        events.addAll(newEvents);
        events.sort(Event::compareByTime);
      }
    } else if (currentType == EventType.PROGRESS) {
      final Event newEvent = manager.sendOrderToDevice(currentTime, currentEvent);
      if (newEvent != null) {
        events.add(newEvent);
        events.sort(Event::compareByTime);
      }
    } else if (currentType == EventType.COMPLETED) {
      computers.get(currentId).release(currentTime);
      events.add(new Event(EventType.PROGRESS, currentTime, null));
      events.sort(Event::compareByTime);
    }
  }

  public void auto() {
    while (!events.isEmpty()) {
      stepMode();
    }
  }

}

