package statistic;

import lombok.Getter;

@Getter
public class ClientStatistics {
  private int generatedOrdersCount;
  private int canceledOrdersCount;
  private double totalTime;
  private double totalOnBufferTime;
  private double totalOnComputerTime;
  private double squaredTotalOnBufferTime;
  private double squaredTotalOnComputerTime;

  public ClientStatistics() {
    generatedOrdersCount = 0;
    canceledOrdersCount = 0;
    totalTime = 0;
    totalOnBufferTime = 0;
    totalOnComputerTime = 0;
    squaredTotalOnBufferTime = 0;
    squaredTotalOnComputerTime = 0;
  }

  public void incrementGeneratedOrders() {
    generatedOrdersCount++;
  }

  public void incrementCanceledOrders() {
    canceledOrdersCount++;
  }

  public void addTotalTime(final double time) {
    totalTime += time;
  }

  public void addOnBufferTime(final double time) {
    totalOnBufferTime += time;
    squaredTotalOnBufferTime += time * time;
  }

  public void addOnComputerTime(final double time) {
    totalOnComputerTime += time;
    squaredTotalOnComputerTime += time * time;
  }

  public double getOnBufferDispersion() {
    return (squaredTotalOnBufferTime / generatedOrdersCount - Math.pow(totalOnBufferTime / generatedOrdersCount, 2));
  }

  public double getOnComputerDispersion() {
    return (squaredTotalOnComputerTime / generatedOrdersCount - Math.pow(totalOnComputerTime / generatedOrdersCount, 2));
  }
}