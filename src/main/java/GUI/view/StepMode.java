package GUI.view;

import GUI.Waveform;
import GUI.actions.AutoMode;
import GUI.actions.NextStep;
import program.Controller;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class StepMode {
  final Controller controller;
  final Waveform waveform;

  public StepMode(final Controller controller,
                  final Waveform waveform) {
    this.controller = controller;
    this.waveform = waveform;
  }

  public void start() {
    JFrame currentFrame = new JFrame() {
    };
    currentFrame.setVisible(true);
    currentFrame.setTitle("Пошаговый режим");
    currentFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    currentFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
    currentFrame.setLayout(null);

    String[] bufferTableColumnNames = {"Index", "Insert", "Order"};
    String[][] bufferTableData = new String[Controller.statisticsPub.getBufferSize()][3];
    for (int i = 0; i < controller.getBuffer().getOrders().size(); i++) {
      bufferTableData[i][0] = String.valueOf(i);
    }
    bufferTableData[0][1] = "~~>";
    JTable bufferTable = new JTable(new DefaultTableModel(bufferTableData, bufferTableColumnNames));
    DefaultTableModel bufferTableModel = (DefaultTableModel) bufferTable.getModel();
    bufferTable.setMaximumSize(new Dimension(200, 200));

    String[] canselTableColumnNames = {"order"};
    String[][] canselTableData = new String[1][1];
    JTable canselTable = new JTable(new DefaultTableModel(canselTableData, canselTableColumnNames));
    DefaultTableModel canselTableModel = (DefaultTableModel) canselTable.getModel();
    canselTable.setMaximumSize(new Dimension(200, 200));

    String[] devicesTableColumnNames = {"Index", "Order"};
    String[][] devicesTableData = new String[Controller.statisticsPub.getComputersCount()][2];
    for (int i = 0; i < Controller.statisticsPub.getComputersCount(); i++) {
      devicesTableData[i][0] = String.valueOf(i);
    }
    JTable devicesTable = new JTable(new DefaultTableModel(devicesTableData, devicesTableColumnNames));
    DefaultTableModel devicesTableModel = (DefaultTableModel) devicesTable.getModel();
    devicesTable.setMaximumSize(new Dimension(200, 200));

    String[] calendarTableColumnNames = {"Time", "info", "Action", "Order", "Successful", "Canceled"};
    String[][] calendarTableData = new String[0][5];
    JTable calendarTable = new JTable(new DefaultTableModel(calendarTableData, calendarTableColumnNames));
    DefaultTableModel resultsTableModel = (DefaultTableModel) calendarTable.getModel();
    calendarTable.getColumnModel().getColumn(0).setMinWidth(150);
    calendarTable.getColumnModel().getColumn(2).setMinWidth(70);

    JButton buttonNext = new JButton(
      new NextStep(controller, bufferTableModel, resultsTableModel,
                         devicesTableModel, canselTableModel, waveform));
    buttonNext.setText("Следующий шаг");
    currentFrame.getContentPane().add(buttonNext).setBounds(850,260,150,30);

    JButton buttonAuto = new JButton(
      new AutoMode(controller, currentFrame));
    buttonAuto.setText("Получение сводной таблицы");
    currentFrame.getContentPane().add(buttonAuto).setBounds(1000,260,220,30);

    currentFrame.getContentPane().add(new JScrollPane(waveform.getJPanel()))
            .setBounds(10, 300, 1500, 500);

    currentFrame.getContentPane().add(new JLabel("Буфер"))
            .setBounds(820, 20, 300, 15);
    currentFrame.getContentPane().add(new JScrollPane(bufferTable))
            .setBounds(820, 40, 300, 200);

    currentFrame.getContentPane().add(new JLabel("Компьютеры"))
            .setBounds(1140, 20, 300, 15);
    currentFrame.getContentPane().add(new JScrollPane(devicesTable))
            .setBounds(1140, 40, 300, 100);

    currentFrame.getContentPane().add(new JLabel("Отказано"))
            .setBounds(1140, 150, 300, 15);
    currentFrame.getContentPane().add(new JScrollPane(canselTable))
            .setBounds(1140, 170, 300, 50);

    currentFrame.getContentPane().add(new JLabel("Информация"))
            .setBounds(10, 20, 300, 15);
    currentFrame.getContentPane().add(new JScrollPane(calendarTable))
            .setBounds(10, 40, 800, 250);

    currentFrame.revalidate();
  }
}
