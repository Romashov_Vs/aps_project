package GUI.view;

import lombok.Getter;
import GUI.actions.SetProperties;

import javax.swing.*;
import java.util.ArrayList;

@Getter
public class InputProperties {
  private final ArrayList<JTextField> propertiesConfirm = new ArrayList<>();
  private final int elementCount = 7;

  public void start() {
    JFrame currentFrame = new JFrame() {
    };
    currentFrame.setVisible(true);
    currentFrame.setTitle("Характеристики");
    currentFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    currentFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
    currentFrame.setSize(600, 400);
    currentFrame.setLocation(500, 150);
    currentFrame.setLayout(null);

    currentFrame.getContentPane().add(new JLabel("Количество устройств: "))
            .setBounds(10, 60, 200, 30);
    currentFrame.getContentPane().add(new JLabel("Количество заказчиков: "))
            .setBounds(10, 90, 200, 30);
    currentFrame.getContentPane().add(new JLabel("Время симуляции: "))
            .setBounds(10, 120, 200, 30);
    currentFrame.getContentPane().add(new JLabel("Размер буфера: "))
            .setBounds(10, 150, 200, 30);
    currentFrame.getContentPane().add(new JLabel("Минимальное время на обработку заявки: "))
            .setBounds(10, 180, 260, 30);
    currentFrame.getContentPane().add(new JLabel("Максимальное время на обработку заявки: "))
            .setBounds(10, 210, 260, 30);
    currentFrame.getContentPane().add(new JLabel("Лямбда для генерации: "))
            .setBounds(10, 240, 200, 30);

    ArrayList<JTextField> properties = new ArrayList<>(this.elementCount);
    properties.add(new JTextField("3", 10));
    currentFrame.getContentPane().add(properties.get(0))
            .setBounds(215, 60, 200, 30);
    properties.add(new JTextField("10", 10));
    currentFrame.getContentPane().add(properties.get(1))
            .setBounds(215, 90, 200, 30);
    properties.add(new JTextField("100", 10));
    currentFrame.getContentPane().add(properties.get(2))
            .setBounds(215, 120, 200, 30);
    properties.add(new JTextField("4", 10));
    currentFrame.getContentPane().add(properties.get(3))
            .setBounds(215, 150, 200, 30);
    properties.add(new JTextField("0.1", 10));
    currentFrame.getContentPane().add(properties.get(4))
            .setBounds(275, 180, 140, 30);
    properties.add(new JTextField("0.2", 10));
    currentFrame.getContentPane().add(properties.get(5))
            .setBounds(275, 210, 140, 30);
    properties.add(new JTextField("5", 10));
    currentFrame.getContentPane().add(properties.get(6))
            .setBounds(215, 240, 200, 30);

    for (int i = 0; i < this.elementCount; i++) {
      propertiesConfirm.add(properties.get(i));
    }

    final JLabel title = new JLabel("Введите данные:");
    currentFrame.getContentPane().add(title).setBounds(10, 10, 200, 30);

    final JButton continueBtn = new JButton(new SetProperties(currentFrame, propertiesConfirm));
    continueBtn.setText("Подтвердить...");
    currentFrame.getContentPane().add(continueBtn).setBounds(215, 280, 150, 30);

    currentFrame.revalidate();
  }
}
