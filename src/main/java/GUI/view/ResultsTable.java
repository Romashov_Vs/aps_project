package GUI.view;

import program.Controller;
import statistic.ClientStatistics;

import javax.swing.*;
import java.awt.*;


public class ResultsTable {
  final Controller controller;

  public ResultsTable(final Controller controller) {
    this.controller = controller;
  }

  public void start() {
    JFrame currentFrame = new JFrame() {
    };
    currentFrame.setVisible(true);
    currentFrame.setTitle("Сводная таблица");
    currentFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    currentFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
    String[] columnNames = {"Номер заказчика","Кол-во заказов", "% отказа", "Среднее время в программе",
      "Среднее время ожидания", "Среднее время обслуживания", "Дисперсия T.ож", "Дисперсия Т.обсл","Компьютер", "Эффективность компьютера"};

    String[][] data = new String[Controller.statisticsPub.getClientsCount()][10];
    int i = 0;
    for (ClientStatistics clientStat : controller.getStatistics().getClientsStats()) {
      data[i][0] = String.valueOf(controller.getClients().get(i).getClientId());
      data[i][1] = String.valueOf(clientStat.getGeneratedOrdersCount());
      data[i][2] = String.valueOf((double) clientStat.getCanceledOrdersCount() / clientStat.getGeneratedOrdersCount());
      data[i][3] = String.valueOf(clientStat.getTotalTime() / clientStat.getGeneratedOrdersCount());
      data[i][4] = String.valueOf(clientStat.getTotalOnBufferTime() / clientStat.getGeneratedOrdersCount());
      data[i][5] = String.valueOf(clientStat.getTotalOnComputerTime() / clientStat.getGeneratedOrdersCount());
      data[i][6] = String.valueOf(clientStat.getOnBufferDispersion());
      data[i][7] = String.valueOf(clientStat.getOnComputerDispersion());
      if (i < Controller.statisticsPub.getComputersCount()) {
        data[i][8] = String.valueOf(i);
        data[i][9] = String.valueOf(
                Controller.statisticsPub.getDevicesWorkTime().get(i) / controller.getCurrentTime());
      }
      i++;
    }
    JTable table = new JTable(data, columnNames);
    JScrollPane scroll = new JScrollPane(table);
    table.setPreferredScrollableViewportSize(new Dimension(500, 900));
    currentFrame.getContentPane().add(scroll);
    currentFrame.revalidate();
  }
}
